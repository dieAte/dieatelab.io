import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: 'https://dieate.gitlab.io',
  outDir: 'public',
  publicDir: 'static',
});